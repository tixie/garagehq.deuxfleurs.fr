+++
title="PhD offering to work on Garage and Distributed Systems"
date=2024-01-10
+++

*Deuxfleurs and IMT Atlantique are partnering to fund a PhD student to work on
Garage and distributed systems theory during three years.  The recruitment
process is open and we are currently looking for candidates. Applications are
accepted until Jan 31, 2024.  Read for details.*

<!-- more -->

---


Deuxfleurs and IMT Atlantique are partnering to fund a PhD student to work on
Garage and distributed systems theory, as part of the SEED PhD program, and we
are looking for a candidate. This is a French PhD so the program duration is 3
years, starting in September 2024, and the student is expected to already have
a master's degree or to obtain one before September 2024.  The PhD will take
place mostly at IMT Atlantique in Nantes (France) within the STACK team, with a
three-month stay at Deuxfleurs and a three-month stay abroad (probably in the
US). Ideally we are looking for a candidate that already has solid Rust coding
skills and a good understanding of distributed systems theory, however both
skills can be learnt during the program. Dr. Alex Auvolat from Deuxfleurs will
be supervising the student along with Dr. Daniel Balouek from IMT Atlantique.
This is a great opportunity to improve your Rust coding skills, learn
distributed systems theory, travel to France, meet the great people behind
Deuxfleurs and, incidentally, obtain a diploma. Feel free to apply or pass the
information to anyone you know who might be interested.

- Read the [PhD topic proposal](https://www.imt-atlantique.fr/sites/default/files/recherche/doctorat/seed/research-topics/6-consensus-algorithms.html)

- For more context, also read the following blog posts:

  - [Maintaining read-after-write consistency in all circumstances](@/blog/2023-12-preserving-read-after-write-consistency/index.md)
  - [Thoughts on "Leaderless Consensus"](@/blog/2023-11-thoughts-on-leaderless-consensus/index.md)

- [Apply here](https://www.imt-atlantique.fr/en/research-innovation/phd/seed/application)

- Read about [the SEED PhD program](https://www.imt-atlantique.fr/en/research-innovation/phd/seed)

- Read about [the STACK team at IMT Atlantique](https://stack-research-group.gitlabpages.inria.fr/web/)

A webinar will be held on Friday, Jan 10, 2024, at 11:00 CET, to introduce
  the PhD subject and the context. See details at [this
  address](https://www.imt-atlantique.fr/en/research-innovation/phd/seed/events#webinars) (we are subject 6-consensus-algorithms).

The PhD topic is open for applications until Jan 31, 2024.
