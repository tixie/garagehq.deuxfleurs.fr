# Garage Website

¡ Work in progress (almost done) !

---

## Setup

- Install Zola with `pacman -S zola`
- Clone this repo
- Run `npm install` to get the dev dependencies
- Run `zola build` to get the public directory
- Run `npm start` to compile styles and scripts
- Run `zola serve`

## Build

CSS : `28.4 kB`

JS : `6.8 kB (app)` + `1.2 MB (search)`*

*<em>The search index in loaded only when the user opens the search modal</em>

Images + Icons : `1.1 MB`

## Fonctions & utilities

JavaScript can be disabled and the website will still run nicely.
It only brings QoL advantages for the user.

The function is [x] if it still runs <u>without</u> JavaScript enabled.

- [x] Responsive main navigation menu (toggle)
- [x] Documentation : user can deploy or reploy ToC submenus
- [ ] Documentation : deploy only the current ToC submenu after a page change
- [ ] Documentation : sidebar focus effect on current section anchor when user scrolls
- [ ] Documentation : ToC that follows the user's scroll
- [ ] Global search

## Screenshots

<a target="_blank" href="https://git.deuxfleurs.fr/sptl/garage_website/raw/branch/master/static/screenshots/screenshot-480.png">480px</a>

<a target="_blank" href="https://git.deuxfleurs.fr/sptl/garage_website/raw/branch/master/static/screenshots/screenshot-768.png">768px</a>

<a target="_blank" href="https://git.deuxfleurs.fr/sptl/garage_website/raw/branch/master/static/screenshots/screenshot-1280.png">1280px</a>
